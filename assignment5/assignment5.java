/*
Q5. Write a program to check the input characters for uppercase, 
lowercase, number of digits and other characters. Display appropriate 
message.
*/


package assignment5;

import java.util.ArrayList;
import java.util.Scanner;

public class assignment5 {

	public static void main(String[] args) {

		System.out.println("Enter string");
		Scanner sc = new Scanner(System.in);
		String input = sc.next();
		char[] input1 = input.toCharArray();
		ArrayList<Character> upper = new ArrayList<Character>();
		ArrayList<Character> lower = new ArrayList<Character>();
		ArrayList<Character> digits = new ArrayList<Character>();
		ArrayList<Character> other = new ArrayList<Character>();
	
		for(int i=0;i<input.length();i++) 
		{
		if((input1[i]>=65)&&(input1[i]<=90))
			upper.add(input1[i]);
		else if((input1[i]>=97)&&(input1[i]<=122))
			lower.add(input1[i]);
		else if((input1[i]>=48)&&(input1[i]<=57))
			digits.add(input1[i]);
		else
			other.add(input1[i]);	
	}
		System.out.println("Upper"+upper);
		System.out.println("Lower"+lower);
		System.out.println("Digits"+digits);
		System.out.println("Other"+other);

}
}
