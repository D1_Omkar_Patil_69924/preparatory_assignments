/*
 Q4. Write a program to calculate the grade of a student. There are five 
subjects. Marks in each subject are entered from keyboard. Assign grade 
based on the following rule:
*/

package assignment4;

import java.util.Scanner;

public class assignment4 {

	public static void main(String[] args) {
		
		int[] marks = new int[5];
		System.out.println("Enter the marks");
		Scanner sc = new Scanner(System.in);
		for(int i=0;i<5;i++)
			marks[i] = sc.nextInt();
		
		System.out.println("Marks you entered are :");
		for(int i=0;i<5;i++)
			System.out.print(marks[i]+" ");
		
		int total = marks[0];
		for(int i=1;i<+5;i++)
			total = total + marks[i];
		System.out.println("\nTotal marks :"+total);
		
		if(total>=90)
			System.out.println(" Grade : Ex");
		else if((total>90)&&(total<=80)) 
		{
			System.out.println(" Grade : A");
		}
		else if((total>80)&&(total<=70)) 
		{
			System.out.println(" Grade : B");
		}
		else if((total>70)&&(total<=60)) 
		{
			System.out.println(" Grade : C");
		}
		else if(total<60)
		{
			System.out.println(" Grade : F");
		}		
		
	}

}
