// Q14 Write a program to check if give string is palindrome.

package Assignment_14;

import java.util.Scanner;

public class Assignment_14 {

	public static void main(String[] args) {
		
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		int length = str.length();
		boolean flag = false;
		for(int i=0;i<=length;i++)
		{
			if(str.charAt(i)!=str.charAt(--length)) 
			{
				flag = false;
				break;
			}
			else
				flag = true;
			
		}
		
		if(flag == true)
			System.out.println("Palindrome");
		else
			System.out.println("Not palindrome");

	}

}
