package Assignment_8;

import java.util.Scanner;

class Student
{
	String name;
	String rollno;
	int marks;
	
	Student()
	{
		name = "Default";
		rollno = "000";
		marks = 000;
	}
	
	void display()
	{
		System.out.println("Student name : "+this.name);
		System.out.println("Student rollno : "+this.rollno);
		System.out.println("Student marks : "+this.marks);
	}
	void accept() 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name ");
		this.name = sc.next();
		
		System.out.println("Enter rollno ");
		this.rollno = sc.next();
		
		System.out.println("Enter marks ");
		this.marks = sc.nextInt();
	}
}
public class assignment_8 {

	public static void main(String[] args) {
		
		System.out.println("How many students's data you wish to enter?");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		Student[] data = new Student[n];
		for(int i=0;i<n;i++)
			data[i] = new Student();
		
		for(int i=0;i<n;i++)
		{
			System.out.println("Enter data of student"+(i+1));
			data[i].accept();
		}

		for(int i=0;i<n;i++)
		{
			System.out.println("\nData you entered of student "+i);
			data[i].display();
		}

		
	}

}
