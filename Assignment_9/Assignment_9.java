/*
 Q9. Accept an integer number and when the program is executed print the 
binary, octal and hexadecimal equivalent of the given number.
 */

package Assignment_9;

import java.util.Scanner;

public class Assignment_9 {

	public static void main(String[] args) {
		
		System.out.println("Enter the number");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println("Binary of given number :"+Integer.toBinaryString(num));
		System.out.println("Octal of given number :"+Integer.toOctalString(num));
		System.out.println("Hexadecimal of given number :"+Integer.toHexString(num));

	}

}
