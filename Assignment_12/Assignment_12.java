
// Q12. Write a Program to reverse the letters present in the given String.

package Assignment_12;

import java.util.Scanner;

public class Assignment_12 {

	public static void main(String[] args) {
		
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		StringBuilder str = new StringBuilder(sc.next());
		str.reverse();
		System.out.println("Reverse of string you entered : "+str);

	}

}
