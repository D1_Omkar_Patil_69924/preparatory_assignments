/*
Q15. Input a string from the user. Count occurrences (case insensitive) of 
each alphabet in the string.

 */
package Assignment_15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Assignment_15 {
	
	static boolean search(char ch, ArrayList<Character> visited)
	{
		for(Character c:visited) 
		{
			if(c==ch)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		
		System.out.println("Enter the string");
		LinkedHashMap<Character,Integer> map = new LinkedHashMap<Character,Integer>();
		Scanner sc = new Scanner(System.in);
		String input = sc.next();
		char[] chr = input.toCharArray();
		Arrays.sort(chr);
		System.out.println(chr);
		String sorted = new String(chr);
		String str = sorted.toLowerCase();
		System.out.println("str :"+str);
		char temp;
		int val;
		ArrayList<Character> visited = new ArrayList<Character>();
		for(int i=0;i<str.length();i++)
		{
			if(search(str.charAt(i),visited))
				continue;
			else
			{
				temp = str.charAt(i);
				val = 0;
				
				for(int j=0;j<str.length();j++)
				{
					if(temp == str.charAt(j))
						val++;	
				}
				
				visited.add(str.charAt(i));
				map.put(str.charAt(i), val);
				
			}
			
			
		}

		System.out.println(map);

		
	}

}
