/*
Q13. Declare an Array of type String. Display the strings which are 
duplicated in that array. (Hint: use equals())

 */
package Assignment_13;

import java.util.ArrayList;

public class Assignmnet_13 {
	
	static boolean search(String str,ArrayList<String> duplicate)// Checks if duplicate string is 
	{															// already present in list or not.
		for(String s:duplicate)
		{
			if(str.equals(s))
				return false;
		}
		
		return true;
	}

	public static void main(String[] args) {
		
		String str[] = {"ab" , "a" , "ab" ,"ac","ab","ac"};
		ArrayList<String> duplicate = new ArrayList<String>();
		
		for(int j=0;j<=str.length-2;j++)
		{
			for(int i=j+1; i<=str.length-1 ;i++) 
			{
				if(str[j].equals(str[i]))
				{
					if(search(str[j],duplicate))
						duplicate.add(str[j]);
				}
					
				
			}
		}
		
		System.out.println(duplicate);

	}

}
