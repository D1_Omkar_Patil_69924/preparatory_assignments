package assignment2;
import java.util.Scanner;

/*
 Q2. Write a program to calculate a Factorial of a number
 */

public class assignment2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();
		int fact = 1;
		for(int i=1;i<=num;i++)
			fact = fact * i; 
		System.out.println("Factorial is :"+fact);

	}

}
