
/*
Q10. Read at most 10 names of students and store them into an array of 
String nameOfStudents[10]. Sort the array and display them back. Hint: Use 
Arrays.sort() method.

 */

package Assignment_10;

import java.util.Arrays;
import java.util.Scanner;

public class Assignment_10 {

	public static void main(String[] args) {
		
		String nameOfStudents[] = new String[10];
		Scanner sc = new Scanner(System.in);

		for(int i=0;i<10;i++)
		{
			System.out.println("Enter name of student :"+(i+1));
			nameOfStudents[i] = new String(sc.next());	
		}
				
		Arrays.sort(nameOfStudents);
		System.out.println("Sorted array: ");
		for(int i=0;i<10;i++)
		{
			System.out.print(nameOfStudents[i]+" ");	
		}
	}

}
