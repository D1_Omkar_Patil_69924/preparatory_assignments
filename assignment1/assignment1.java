package assignment1;

/*
 Q1. Write a program to input n numbers on command line argument and calculate maximum of them.
*/

public class assignment1 {

	public static void main(String[] args) {
		int temp = 0;
		for(int i=0;i<=args.length;i++) 
		{
			temp = Integer.parseInt(args[i]);
			if(i==args.length-1)
				break;
			if(temp<Integer.parseInt(args[i+1]))
				temp = Integer.parseInt(args[i+1]);
		}
		
		System.out.println("Maximum number is : "+temp);
	
	}

}
