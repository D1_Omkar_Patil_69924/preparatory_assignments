
//Q6. Write a program to perform matrix multiplication.

package assignment6;

import java.util.Scanner;

public class assignment6 {

	public static void main(String[] args) {
		int arr[][] = new int[3][3];
		int arr2[][] = new int[3][3];
		int product[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first matrix elements");
		for(int i=0;i<3;i++) 
		{
			for(int j=0;j<3;j++)
			{
				arr[i][j] = sc.nextInt(); 
			}
		
		}
		
		for(int i=0;i<3;i++) 
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(arr[i][j]+" "); 
			}
			System.out.println("");
		}
		
		System.out.println("Enter second matrix elements");
		for(int i=0;i<3;i++) 
		{
			for(int j=0;j<3;j++)
			{
				arr2[i][j] = sc.nextInt(); 
			}
		
		}
		
		for(int i=0;i<3;i++) 
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(arr2[i][j]+" "); 
			}
			System.out.println("");
		}
		
		for(int k=0;k<3;k++) 
		{
			for(int i=0;i<3;i++) 
			{
				int temp = 0;
				for(int j=0;j<3;j++) 
				{
					
					temp = temp + arr[k][j]*arr2[j][i];			
				}
				product[k][i] = temp;
			}
			
		}
		System.out.println("Product is :");
		for(int i=0;i<3;i++) 
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(product[i][j]+" "); 
			}
			System.out.println("");
		}
		
	}

}
