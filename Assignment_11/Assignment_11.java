/*
Q11. Create a class called Employee that includes three fields - a first name 
(type String), a last name (type String) and a monthly salary (double). 
Provide a constructor that initializes these fields. Provide a set and a get 
method for each instance variable. If the monthly salary is not positive, do 
not set its value.
 */

package Assignment_11;

import java.util.Scanner;

class Employee
{
	private String firstName;
	private String lastName;
	private double salary;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		if(salary>0)
		this.salary = salary;
	}

	Employee()
	{
		firstName = "Default";
		lastName = "Default";
		salary = 00;
	}
}

