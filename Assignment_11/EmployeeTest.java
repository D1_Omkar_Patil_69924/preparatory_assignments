package Assignment_11;

import java.util.Scanner;

public class EmployeeTest {

public static void main(String[] args) {
		
		Employee e = new Employee();
		
		System.out.println("Information of first employee");
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first name");
		e.setFirstName(sc.next());
		
		System.out.println("Enter last name");
		e.setLastName(sc.next());
		
		System.out.println("Enter salary");
		e.setSalary(sc.nextDouble());
		
		System.out.println("Data you entered:");
		System.out.print("First Name: ");
		System.out.println(e.getFirstName());
		
		System.out.print("Last Name: ");
		System.out.println(e.getLastName());
		
		System.out.print("Salary: ");
		System.out.println(e.getSalary());
		
		//------------------------------------------------------------------
		
		Employee e1 = new Employee();
		
		System.out.println("Information of second employee");
		
		System.out.println("Enter first name");
		e1.setFirstName(sc.next());
		
		System.out.println("Enter last name");
		e1.setLastName(sc.next());
		
		System.out.println("Enter salary");
		e1.setSalary(sc.nextDouble());
		
		System.out.println("Data you entered:");
		System.out.print("First Name: ");
		System.out.println(e1.getFirstName());
		
		System.out.print("Last Name: ");
		System.out.println(e1.getLastName());
		
		System.out.print("Salary: ");
		System.out.println(e1.getSalary());
		
		//-----------------------------------------------------------------------------
		
		System.out.println("After 10% raise in salary ,");
		
		System.out.println("Salary of first employee :"+e.getSalary()*1.1);
		
		System.out.println("Salary of second employee :"+e1.getSalary()*1.1);
		


	}


}
