package assignment3;

import java.util.Scanner;

/*
 Q3. Write a program to calculate Fibonacci Series up to n numbers
 */
public class assignment3 {

	public static void main(String[] args) {
		
		System.out.println("Enter the number of elements in series");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int first = 1;
		int second = 2;
		int temp = 0;
		System.out.print(first+" "+second+" ");
		for(int i = 0;i<num-2;i++)
		{
		if(num == 2)
			break;
		temp = first + second;
		first = second;
		second = temp;
		System.out.print(temp + " ");
		}
		
	}

}
