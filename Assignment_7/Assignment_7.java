/*
Q7. Write a program to accept a number from user using command line 
argument and display its table.
*/

package Assignment_7;

public class Assignment_7 {

	public static void main(String[] args) {
		int num = Integer.parseInt(args[0]);
		for(int i=0;i<10;i++)
		{
			System.out.println(num*(i+1));
		}

	}

}
